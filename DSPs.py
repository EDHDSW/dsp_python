

from math import*

#multiply kernels
def convolution(input_signal,kernel):
    signal_size=len(input_signal)
    kernel_size=len(kernel)
    output_signal=[0]*(signal_size+kernel_size-1)
    for x in xrange(signal_size):
        for h in xrange(kernel_size):
            output_signal[x+h]=output_signal[x+h]+kernel[h]*input_signal[x]
    return output_signal

#dsp Convolution deprecating the extra inforamtion
def convolution_const(input_signal,kernel):
    signal_size=len(input_signal)
    kernel_size=len(kernel)
    output_signal=[0]*(signal_size+kernel_size-1)
    for x in xrange(signal_size):
        for h in xrange(kernel_size):
            output_signal[x+h]=output_signal[x+h]+kernel[h]*input_signal[x]
    output_signal=output_signal[:signal_size]
    return output_signal

#DFT calculated by correlation
def DFT(time_signal):
    signal_size=len(time_signal)
    fourier_size = (signal_size//2)+1
    #N=signal_size
    real_part=[0]*fourier_size
    imaginary_part=[0]*fourier_size
    for x in xrange(fourier_size):
        for h in xrange(signal_size-1):
            real_part[x] = real_part[x] + (time_signal[h]*cos((2*pi*x*h)/signal_size))
            imaginary_part[x] = imaginary_part[x] - (time_signal[h]*sin((2*pi*x*h)/signal_size))
    return real_part,imaginary_part

#inverse DFT
def IDFT(real_part,imaginary_part):
    fourier_size =len(real_part)
    signal_size = (fourier_size -1 )*2
    time_signal=[0]*signal_size
    #N=signal_size
    N2=signal_size//2
    for x in xrange(fourier_size):
        real_part[x]=real_part[x]/N2
        imaginary_part[x]=-imaginary_part[x]/N2
    real_part[0]=real_part[0]/2
    real_part[fourier_size-1]/2
    for x in xrange(signal_size-1):
        for h in xrange(fourier_size):
            time_signal[x]=time_signal[x] + real_part[h]*cos((2*pi*x*h)/signal_size) +\
                            imaginary_part[h]*sin((2*pi*x*h)/signal_size)
    return time_signal

#convert to polar notation
def toPolar(real_part,imaginary_part):
    fourier_size =len(real_part)
    magnitude =[0]*fourier_size
    phase = [0]*fourier_size
    for k in xrange(fourier_size):
        magnitude[k]= sqrt(real_part[k]**2 +imaginary_part[k]**2)
#        phase [k] =atan(imaginary_part[k]/real_part[k])
        phase [k] =atan2(imaginary_part[k],real_part[k])
    return magnitude,phase

def toRectangular(magnitude,phase):
    fourier_size = len(magnitude)
    real_part=[0]*fourier_size
    imaginary_part=[0]*fourier_size
    for k in xrange(fourier_size):
        real_part[k]=magnitude[k]*cos(phase[k])
        imaginary_part=magnitude[k]*sin(phase[k])
    return real_part,imaginary_part

def DFTcomplex(partr,parti):
    N= len(partr)
    real_part=[0]*N
    imaginary_part=[0]*N
    for k in xrange(0,N,1):
        for i in xrange(0,N,1):
            sr =cos(2*pi*k*i/N)
            si=-sin(2*pi*k*i/N)
            real_part[k]=real_part[k] +partr[i]*sr -parti[i]*si
            imaginary_part[k]=imaginary_part[k]+partr[i]*si +parti[i]*sr
    return real_part,imaginary_part

def FFT(real,imag,sing):
    Wr=0.0 #real part from e**(-j 2 pi/ N)
    Wi=0.0 #imaginary part from e**(-j 2 pi/ N)
    Ur=0.0 #real part from twiddle factor W**k
    Ui=0.0 #imaginary part from twiddle factor W**k
    tempR=0.0 #real part for temporary storage
    tempI=0.0 #imaginary part for temporary storage
    N= len(real)
    N2=N>>1#N/2
    M=int(log(N)/log(2))#stages
    L=0#FFT stage
    LE=0#Number of point in sub DFT at stage L and offset to next DFT in Stage
    LE1=0 #number of butterlies in one DFT at stage L.
    j=0
    for i in xrange(1,(N-1),1):
        '''
        Increment bit-reverser counter j by addind 1 to msb
        and propagating carries from left to right
        '''
        k=N2#k is the 1 in msb,0 elsewhere
        #propagate carry from left to right
        while(k<=j):
            j= j-k #bit tested is 1 so clear it 
            k=k>>1#k/2 #set up 1 for next bit to right
        j=j+k #change 1st 0 from left to 1            
        if(i<j):#test if previously swapped
            tempr=real[j]
            real[j]=real[i]
            real[i]=tempr
            tempi=imag[j]
            imag[j]=imag[i]
            imag[i]=tempi
    # do M stages butterflies
    for L in xrange(1,(M+1),1):
        LE= 1<<L #2**L
        LE1= LE>>1 #LE/2
        Ur = 1.0#U=1+j0
        Ui = 0.0
        Wr = cos(pi/LE1)
        Wi = sing*sin(pi/LE1)#from euler formulas W= e**(-j 2 pi/LE)
        #Do the butterflies for L-th stage
        #Do the LE1 butterflies per sub DFT
        for j in xrange(0,LE1,1):
            for i in xrange(j,N,LE):
                ID = i+LE1 #index of lower poitn butterfly
                tempR = real[ID] *Ur - imag[ID]*Ui
                tempI = imag[ID] *Ur + real[ID]*Ui
                real[ID] = real[i] - tempR
                imag[ID] = imag[i] - tempI
                real[i] = real[i] + tempR
                imag[i] = imag[i] + tempI
            tempR= Ur*Wr - Ui*Wi
            Ui = Ur*Wi + Ui*Wr
            Ur = 1.0*tempR
    #Normalize if its inverse
    if(sing == 1):
        for i in xrange(0,N,1):
            real[i]=real[i]/N
            imag[i]=imag[i]/N
    return real,imag
#spectrum
def Spectrum(p1,p2,N,S):
    output =[]
    N2 = N/2
    real= 1*p1[:N]
    img = 1*p1[:N]
    FFT(real,img,-1)
    for i in xrange(0,N2,1):
        #IN(w) =1/N*|Y (w)|**2
        output.append(sqrt( ( (real[i]**2)+(img[i]**2) ) ) )
        #normalization
        output[i] = output[i]/(S*2)# sampling  number * number of spectrums buffers added
    return output;
#Convolution usign FFT
class FFTConvolution(object):
    def __init__(self):
        self.kernel =[]
        self.signal_real=[]
        self.signal_imag=[]
        self.signal_size=0
        self.overlap_size=0
        self.overlap =0
        self.is_multi=0
        self.number_segments=0
        self.multisignal_size=0
        
    def set_single(self,kernel,signal_size):
        self.kernel = kernel
        self.signal_size=signal_size
        self.kernel_size = len(self.kernel)
        self.FFT_size = self.kernel_size + self.signal_size-1 
        #pad the kernel with zeros
        zero_padding=[0]*(self.FFT_size-self.kernel_size)
        self.overlap =[0]*(self.FFT_size-self.signal_size)
        self.kernel_real=self.kernel+zero_padding
        self.kernel_imag=[0]*(self.FFT_size)
        #calcualte the kernel FFT
        FFT(self.kernel_real,self.kernel_imag,-1)
        self.signal_imag=[0]*self.FFT_size
        self.overlap_size =len(self.overlap)
        self.is_multi=0
        
    def set_multi(self,kernel,signal_size,segment_size):
        #size of the complete signal
        self.multisignal_size = signal_size
        self.kernel = kernel
        #segment size its the signal size of each indiviudal segment of signal 
        self.signal_size = segment_size
        self.kernel_size = len(self.kernel)
        self.FFT_size = self.kernel_size + self.signal_size -1
        self.number_segments = self.multisignal_size/self.signal_size
        #pad the kernel with zeros
        zero_padding=[0]*(self.FFT_size-self.kernel_size)
        self.overlap =[0]*(self.FFT_size-self.signal_size)
        self.kernel_real=self.kernel+zero_padding
        self.kernel_imag=[0]*(self.FFT_size)
        #calcualte the kernel FFT
        FFT(self.kernel_real,self.kernel_imag,-1)
        self.signal_imag=[0]*self.FFT_size
        self.overlap_size = self.FFT_size-self.signal_size
        self.is_multi=1

    def convolve(self,signal):
        output_signal=[]
        #padd the input with zeros
        self.signal_real= signal+ self.overlap
        #calcualte the FFT
        FFT(self.signal_real,self.signal_imag,-1)
        #multiply the r frequency spectrum by the frequency response
        for x in xrange(0,self.FFT_size,1):
            temp = self.signal_real[x]*self.kernel_real[x] - self.signal_imag[x]*self.kernel_imag[x]
            self.signal_imag[x]=self.signal_real[x]*self.kernel_imag[x] + self.signal_imag[x]*self.kernel_real[x]
            self.signal_real[x]=temp
        #Inverse FFT
        FFT(self.signal_real,self.signal_imag,1)
        #short the answer
        output_signal=self.signal_real[:self.signal_size]
        #if the FFT its multi 
        if (0 == self.is_multi):
            #save the extra information
            self.overlap=self.signal_real[self.signal_size:]
            #add the overalap segment
            for x in xrange(0, self.overlap_size,1):
                output_signal[x]=output_signal[x]+self.overlap[x]
        return output_signal
    
    def convolveMulti(self,signal):
        work_signal=[]
        work_signal=signal[0:self.signal_size]
        #first segment the output signal its the same size as input signal
        output_signal=self.convolve(work_signal)
        for x in xrange(1,self.number_segments,1):
            index = x*(self.signal_size)
            work_signal=signal[index:(x+1)*self.signal_size]
            output_signal = output_signal + self.convolve(work_signal)
        print 'FFT done! Signal size',self.multisignal_size,'Number Segments',self.number_segments
        return output_signal
    
#FILTERS
# FIR FILTER 
class FilterMovingAverage(object):
    def __init__(self,size):
        self.filter_size=size
        self.filter_kernel=[]
        
    def set_kernel(self,kernel):
        self.filter_kernel=kernel
        
    def set_size(self,size):
        self.filter_size=size
        
#Recursive implementation
    def dspfilter(self,input_signal):
        #IIR implementation
        signal_size=len(input_signal)
        output_signal=[0]*signal_size
        offset=self.filter_size//2
        acc=0
        p=(self.filter_size-1)//2
        q=p+1
        for x in xrange(self.filter_size):
            output_signal[x]=input_signal[x]
            acc=acc+input_signal[x]
        #got the first average
        output_signal[offset]=acc//self.filter_size
        for x in xrange(offset+1,signal_size-offset):
            output_signal[x]=output_signal[x-1]+input_signal[x+p]-input_signal[x-q]
        for x in xrange(signal_size-offset,signal_size):
             output_signal[x]=input_signal[x]   
        return output_signal
    
#convolution inmplementation                              
    def filter_by_convolution(self,input_signal):
        return convolution(input_signal,self.filter_kernel)
    
#Gaussian 
    def Multifilter(self,input_signal,times):
        output_signal=[]
        for x in xrange(times):
            output_signal=self.dspfilter(input_signal);
        return output_signal

# fcHigh and fcLow are cuttof frequency expressed in fraction
# (0.0-0.5)of the sampling rate
# Kernel size its calculated by the relation M= 4/BW
# BW its the transition band expressed inf fraction of sampling rate
class FilterWindowSinc(object):
    def __init__(self):
        
        self.fcWork=0.0
        self.kernel_size=0.0
        self.kernel=[]
        self.kernel_buffer=[]
        self.K=0.0
        
    def kernel_clc(self,kernel):
        k2 =self.kernel_size/2.0
        pi2=2*pi
        pi4=4*pi
        kernel_valAux= 2*pi*self.fcWork
        for x in xrange(0,self.kernel_size,1):
            #check for zero division
            if (0 == ( x - k2) ):
                kernel[x] = kernel_valAux
            else:
                kernel[x] = sin(pi2*self.fcWork*(x-k2))/(x-k2)
            kernel[x] = kernel[x] *(0.42 - 0.5*cos(pi2*x/self.kernel_size) + 0.08*cos(pi4*x/self.kernel_size))
            #calculate the normalize factor
            self.K= self.K + kernel[x]
        #normalize

        for x in xrange(0,self.kernel_size,1):
            if(self.K != 0):
                kernel[x] = kernel[x]/self.K
        
 
    def kernel_inversion(self,kernel):
        kernel_half= self.kernel_size//2
        for x in xrange(0,self.kernel_size,1):
            kernel[x] = -1* kernel[x]
        kernel[kernel_half] = kernel[kernel_half] +1
        
            
    def setlowpass(self,fcLow,kernel_size):
        self.kernel=[0]*kernel_size
        self.kernel_size = kernel_size 
        self.fcWork = fcLow
        self.kernel_clc(self.kernel)


    def sethighpass(self,fcHigh,kernel_size):
        self.kernel=[0]*kernel_size
        self.kernel_size = kernel_size
        self.fcWork = fcHigh
        self.kernel_clc(self.kernel)
        self.kernel_inversion(self.kernel)
        
    def setbandreject(self,fcLow,fcHigh,kernel_size):
        #calculate a low pass filters
        sumk=0.0
        self.kernel=[0]*kernel_size
        self.kernel_size = kernel_size
        self.fcWork = fcLow
        self.kernel_clc(self.kernel)
        #calculate a high pass filter
        self.kernel_buffer=[0]*kernel_size
        self.fcWork = fcHigh
        self.kernel_clc(self.kernel_buffer)
        self.kernel_inversion(self.kernel_buffer)
        #add the kernel to create a band reject filter
        for x in xrange(0,self.kernel_size,1):
            self.kernel[x] = self.kernel[x] + self.kernel_buffer[x]
            sumk = sumk +self.kernel[x]
        for x in xrange(0,self.kernel_size,1):
            self.kernel[x] = self.kernel[x]/sumk
             
            
        #save the buffer
        
    def setbandpass(self,fcLow,fcHigh,kernel_size):
        #create a band reject filter
        self.setbandreject(fcLow,fcHigh,kernel_size)
        #inverse ethe filter to create a band pass filter
        self.kernel_inversion(self.kernel)

    def dspfilter(self,signal): 
        output = convolution_const(signal,self.kernel)
        return output
    
    def FFTFilter(self,signal):
        convolver = FFTConvolution()
        convolver.set_single(self.kernel,len(signal))
        output_signal = convolver.convolve(signal)
        return output_signal
    
# IIR FILTER
def recursionR(signal,a,b,Nopoles):
    signal_size=len(signal)
    output=[0]*signal_size
    for x in xrange(Nopoles,signal_size,1):
        for i in xrange(0,Nopoles+1,1):
            bindex = (x - 1) -i
            if (bindex < 0):
                bindex=0
            output[x]=output[x]+a[i]*signal[x-i]+b[i]*output[bindex]
    return output


def recursionRI(signal,a,b,Nopoles):
    signal_size=len(signal)
    output=[0]*signal_size
    reference = signal_size-Nopoles
    for x in xrange(0,reference ,1):
        for i in xrange(0,Nopoles,1):
            bindex = (x + 1) + i
            if (bindex > signal_size ):
                bindex=0
            output[x]=output[x]+a[i]*signal[x+i]+b[i]*output[bindex]
    return output
                            
                            
        
class FilterSinglePole(object):
    def __init__(self):
        self.a=[0]*7
        self.b=[0]*7
        
    def set_filter_values(self,a,b):
        self.a=a
        self.b=b

    def setlowpass(self,x):
        self.a[0]=1.0-x
        self.b[0]=x
        
    def sethighpass(self,x):
        self.a[0]=(1+x)/2.0
        self.a[1]= -1.0*(1+x)/2.0
        self.b[0]= x #b[0]=b1 in math
        
        
    def setlowpass4stage(self,x):
        self.a[0]=(1-x)**4
        self.b[0]= 4*x #b1
        self.b[1]=-6*(x**2)#b2
        self.b[2]=4*(x**3)#b3
        self.b[3]=-1*(x**4)#b4
   
    def dspfilter(self,signal):
        output=[]
        output=recursionR(signal,self.a,self.b,4)#recursion(signal,self.a,self.b)
        return output
    
    def dspfilterbidirectional(self,signal):
        output=[]
        work=[]
        work=recursionR(signal,self.a,self.b,4)
        output=recursionRI(work,self.a,self.b,4)
        return output
    
# IIR FILTER
class FilterNarrowBand(object):
    def __init__(self):
        self.a=[0]*7
        self.b=[0]*7
        
    def set_filter_values(self,a,b):
        self.a=a
        self.b=b

    def setbandpass(self,fcenter,BW):
        R=1-(3*BW)
        K=(1-(2*R*cos(2*pi*fcenter))+(R**2)) / (2-2*cos(2*pi*fcenter))
        self.a[0] = 1-K
        self.a[1]= 2*(K-R)*cos(2*pi*fcenter)
        self.a[2]= (R**2) -K
        self.b[0]= 2*R*cos(2*pi*fcenter)#b[0]=b1
        self.b[1] = -1.0*R**2       

    def setbandreject(self,fcenter,BW):
        R=1-(3*BW)
        K=(1-(2*R*cos(2*pi*fcenter))+(R**2)) / (2-2*cos(2*pi*fcenter))
        self.a[0] = K
        self.a[1]= -2*K*cos(2*pi*fcenter)
        self.a[2]= K
        self.b[0]= 2*R*cos(2*pi*fcenter)#b[0]=b1
        self.b[1] = -1.0*R**2   

    def dspfilter(self,signal):
        output=[]
        output=recursionR(signal,self.a,self.b,4)#recursion(signal,self.a,self.b)
        return output
    
    def dspfilterbidirectional(self,signal):
        output=[]
        work=[]
        work=recursionR(signal,self.a,self.b,4)
        output=recursionRI(work,self.a,self.b,4)
        return output

# IIR FILTER
class FilterChebyshev(object):
    def __init__(self):
        self.a=[]
        self.b=[]
        self.Nopoles=0
        self.PR=0
        self.Fc=0
        #recursion auxiliar coefficients
        self.a0=0.0
        self.a1=0.0
        self.a2=0.0
        self.b1=0.0
        self.b2=0.0

        self.rp=0.0
        self.ip=0.0

#to calculate introduce Number of poles,filter type(low pass or high pass
#,the percentaje of ripple and the cutoff frecuency
    def calc_values(self,Nopoles,filter_type,PR,Fc):
        self.Nopoles=Nopoles
        self.a=[0]*(Nopoles+1)
        self.b=[0]*(Nopoles+1)
        ta=[0]*(Nopoles)
        tb=[0]*(Nopoles)
        self.a[2]=1.0
        self.b[2]=1.0
        self.PR=PR
        self.Fc=Fc
        self.filter_type=filter_type
        for x in xrange(1,Nopoles/2,1):
            self.rp=-1*cos(pi/(Nopoles*2)) +(x-1)*pi/Nopoles
            self.ip=sin(pi/(Nopoles*2)) +(x-1)*pi/Nopoles
            self.calc_ab()
            ta=1*self.a
            tb=1*self.b
            for i in xrange(2,Nopoles+1,1):
                self.a[i]=self.a0*ta[i]+self.a1*ta[i-1]+2*ta[i-2]
                self.b[i]=tb[i]-self.b1*tb[i-1]-self.b2*tb[i-2]
        self.b[2]=0
        for i in xrange(1,Nopoles-2,1):
            self.a[i]=self.a[i+2]
            self.b[i]=-1*self.b[i+2]
        sa=0.0
        sb=0.0
        for i in xrange(1,Nopoles-2,1):
            if filter_type==0:# zero its lo pass
                sa=sa+self.a[i]
                sb=sb+self.b[i]
            else:
                sa=sa + self.a[i]*(-1)**i
                sb=sb + self.b[i]*(-1)**i
        gain=sa/(1-sb)
        for i in xrange(1,Nopoles-2,1):
            if gain != 0:
                self.a[i]=self.a[i]/gain
        
    def calc_ab(self):
        #circle to elipse
        if(self.PR !=0 ):
            es= sqrt( (100/(100-self.PR))**2 -1  )
            vx=(1/ self.Nopoles)*log(1/es)+sqrt(1/(es**2)+1)
            kx=(1/ self.Nopoles)*log(1/es)+sqrt((1/(es**2))-1)
            kx=exp(kx)+exp(-kx)/2
            self.rp=self.rp*((exp(vx)-exp(-vx))/2)/kx
            self.ip=self.ip*((exp(vx)+exp(-vx))/2)/kx
        #z domain to z domain conversion
        t=2*tan(0.5)
        w=2*pi*self.Fc
        m=(self.rp**2) +(self.ip**2)
        d=4-4*self.rp*t + m*(t**2)
        x0=(t**2)/d
        x1=(2*(t**2))/d
        x2=(t**2)/d
        y1=(8-2*m*(t**2))/d
        y2=(-4-4*self.rp*t-m*(t**2))/d

        if self.filter_type==0:# zero its lo pass
            k=sin(0.5-w/2)/sin(.5+w/2)
        else:
            k=-cos(0.5+w/2)/cos(w/2-.5)
        d =1+y1*k-y2*(k**2)
        self.a0=(x0-x1*k+x2*(k**2))/d
        self.a1=(-2*x0*k+x1+x1*(k**2)-2*x2*k)/d
        self.a2=(x0*(k**2)-x1*k+x2)/d
        self.b1=(2*k+y1+y1*(k**2)-2*y2*k)/d
        self.b2=(-(k**2)-y1*k+y2)/d
        if self.filter_type==0:# zero its lo pass
            self.a1=-1*self.a1
            self.b1=-1*self.b1
 
    def set_filter_values(self,a,b,Nopoles):
        self.a=a
        self.b=b
        self.Nopoles=Nopoles
        
    def dspfilter(self,signal):
        size=  len(signal)
        output=[0]*size
        output=recursionR(signal,self.a,self.b,self.Nopoles)
        return output
        
    def dspfilterbidirectional(self,signal):
        output=[]
        work=[]
        work=recursionR(signal,self.a,self.b,self.Nopoles)
        output=recursionRI(work,self.a,self.b,self.Nopoles)
        return output
    
        

    


    
