
'''

|
|
|    |   | 
|   | | | | | |
|  |  |    | | |
| |             |
|----------------------------------

muestreado a 8 khz
0-8000 muestras en un segundo 
12Bits ADC 0- 4096

Estrucutra de un ruido cardiaco
R4 --------R1-T-----------R2-P--------- -R3 
sistole  R1 -R2
diastole R2-R1
R3-R4 ruidos patologicos
T-P chasquidos patologicos
Los soplos se producen entre R1-R2, y R2-R1

Supondremos que en 1 segundo(N numero de muestras muestras) abarcan 2 ciclos completos de sistole,diastole 
R4 --------R1-T-----------R2-P----- R3-----R4-------R1T-------R2

                        
          Rangos Valor ADC     Numero de Muestras 
R1- R2    0 - 3000             N/16    
R4-R3     0 - 1000             N/16 + 100
T-P       0 - 2000             N/64 +50
(R2-R1),(R2-R1) = N/8
Frecuencia = (R2x -R1x )+(R2x - R1x) * 1/8000;  8Hz = frecuencia de  muestreo 
'''
from DSPs import*
import matplotlib.pyplot as plt
from random import randint

#Sampling Frequency = 8khz
#Sampling Time 
t = 100 #in milliseconds
N = 8*t #t*Sampling frequency 
adcY_offset = 300
#smapling ampliude
adcAmplitude = 4096

sizeR1 = N/16
sizeR2 = N/16
sizeR3 = N/16 + N/64
sizeR4 = N/16 + N/64
sizeT = N/64 +N/128
sizeP = N/64 +N/128
sizeR1R2 = N/8
sizeR2R1 = N/8
sizeSmallSpace = N/64
sizeBigSpace = N/16
#soplos
#sistolicos R1-R2
sizePamsistolico_soplo = sizeR1R2
sizeEyectivoA_soplo = sizeR1R2/2 - N/64
sizeEyectivoB_soplo = sizeR1R2/2
sizeProtomesositolico_soplo = sizeR1R2/2
sizeTelesistolico_soplo = sizeR1R2/2
#diastolicos R2-R1
sizeProtodiastolico_soplo = sizeR2R1
sizeMesodiastolicoA_soplo = sizeR2R1/2 - N/64
sizeMesodiastolicoB_soplo = sizeR2R1/2 - N/64
sizeTelediastolico_soplo = sizeR2R1/2
#soplo continuo
sizeContinuo_soploA = sizeR1R2
sizeContinuo_soploB = sizeR2R1/2

R1MaxValue = adcAmplitude/4 + adcAmplitude/2
R2MaxValue = adcAmplitude/4 + adcAmplitude/2
R3MaxValue = adcAmplitude/4
R4MaxValue = adcAmplitude/4
TMaxValue  = adcAmplitude/2
PMaxValue  = adcAmplitude/2
R1R2MaxValue = adcY_offset
R2R1MaxValue = adcY_offset
SmallSpaceMaxValue = adcY_offset
BigSpaceMaxValue = adcY_offset

#soplos
#sistolicos R1-R2
MaxValuePamsistolico_soplo = adcAmplitude/4
MaxValueEyectivoA_soplo = adcAmplitude/4
MaxValueEyectivoB_soplo = adcAmplitude/4
MaxValueProtomesositolico_soplo = adcAmplitude/4
MaxValueTelesistolico_soplo = adcAmplitude/4
#diastolicos R2-R1
MaxValueProtodiastolico_soplo = adcAmplitude/4
MaxValueMesodiastolicoA_soplo = adcAmplitude/4
MaxValueMesodiastolicoB_soplo = adcAmplitude/4
MaxValueTelediastolico_soplo = adcAmplitude/4
#soplo continuo
MaxValueContinuo_soploA = adcAmplitude/4
MaxValueContinuo_soploB = adcAmplitude/4

#slope 0= plane, 1 =positve ,-1 negative

m_R1 = -1 # negative slope
m_R2 = -1
m_R3 = 0 #plane
m_R4 = 0
m_R1R2 = 0
m_R2R1 = 0
m_T  = 1
m_P  = 1
m_SmallSpace = 0
m_BigSpace = 0

#soplos
#sistolicos R1-R2
m_Pamsistolico_soplo = 0
m_EyectivoA_soplo = 1
m_EyectivoB_soplo = -1
m_Protomesositolico_soplo = -1
m_Telesistolico_soplo = 1
#diastolicos R2-R1
m_Protodiastolico_soplo = -1
m_MesodiastolicoA_soplo = 1
m_MesodiastolicoB_soplo = -1
m_Telediastolico_soplo = 1
#soplo continuo
m_Continuo_soploA = 1
m_Continuo_soploB = -1

adcCardiophonogram = [0]* N

#Random generation 
def MapPoint_random(point_x,offset_adcY,MaxValue,x0,slope):
    return randint(offset_adcY,MaxValue)
    
#Equation generation
'''
y = mx - mx0 + y0
m = y1-y0/x1-x0'''
def MapPoint_equation(point_x,offset_adcY,MaxValue,x0,slope):
    y =0
    point = 0
    if slope == 0:
        y = MaxValue     
    if slope > 0:
        y =(slope*point_x) - (slope*x0)  + offset_adcY #y0 = first point in signal in this case    offset_adcY
    else:
        y = (slope*point_x) - (slope*x0)  + (MaxValue)  #y0 = first point in signal in this case    MaxValue
    if y < offset_adcY:
        y = offset_adcY
        
    point = randint(offset_adcY,y)
    
    return point


#functionMap key:equation
functionsDict = {"random_R1":MapPoint_random,
                 "random_R2":MapPoint_random,
                 "random_R3":MapPoint_random,
                 "random_R4":MapPoint_random,
                 "random_T":MapPoint_random,
                 "random_P":MapPoint_random,
                 "random_R1R2":MapPoint_random,
                 "random_R2R1":MapPoint_random,
                 "random_SmallSpace":MapPoint_random,
                 "random_BigSpace":MapPoint_random,
                 "random_Pamsistolico":MapPoint_random,
                 "equation_R1":MapPoint_equation,
                 "equation_R2":MapPoint_equation,
                 "equation_R3":MapPoint_equation,
                 "equation_R":MapPoint_equation,
                 "equation_T":MapPoint_equation,
                 "equation_P":MapPoint_equation,
                 "equation_R1R2":MapPoint_equation,
                 "equation_R2R1":MapPoint_equation,
                 "equation_EyectivoA":MapPoint_equation,
                 "equation_EyectivoB":MapPoint_equation,
                 "equation_Protomesosistolico":MapPoint_equation,
                 "equation_Telesistolico":MapPoint_equation,
                 "equation_Protodiastolico":MapPoint_equation,
                 "equation_MesodiastolicoA":MapPoint_equation,
                 "equation_MesodiastolicoB":MapPoint_equation,
                 "equation_Telediastolico":MapPoint_equation,
                 "equation_ContinuoA":MapPoint_equation,
                 "equation_ContinuoB":MapPoint_equation                 
                 }
#Parameters  key:[MaxValue,size,slope orientation]
ParametersDict = {"random_R1":[R1MaxValue,sizeR1,0],
                  "random_R2":[R2MaxValue,sizeR2,0],
                  "random_R3":[R3MaxValue,sizeR1,0],
                  "random_R4":[R4MaxValue,sizeR2,0],
                  "random_R1R2":[R1R2MaxValue,sizeR1R2,0],
                  "random_R2R1":[R2R1MaxValue,sizeR2R1,0],
                  "random_T":[TMaxValue,sizeT,0],
                  "random_P":[PMaxValue,sizeP,0],
                  "random_SmallSpace":[SmallSpaceMaxValue,sizeSmallSpace,0],
                  "random_BigSpace":[BigSpaceMaxValue,sizeBigSpace,0],
                  "random_Pamsistolico":[MaxValuePamsistolico_soplo ,sizePamsistolico_soplo ,0],
                  "equation_R1":[R1MaxValue,sizeR1,m_R1],
                  "equation_R2":[R2MaxValue,sizeR2,m_R2],
                  "equation_R1R2":[R1R2MaxValue,sizeR1R2,m_R1R2],
                  "equation_R2R1":[R2R1MaxValue,sizeR2R1,m_R2R1],
                  "equation_T":[TMaxValue,sizeT,m_T],
                  "equation_P":[PMaxValue,sizeP,m_P],
                  "equation_EyectivoA":[MaxValueEyectivoA_soplo  ,sizeEyectivoA_soplo  ,m_EyectivoA_soplo  ],
                  "equation_EyectivoB":[MaxValueEyectivoB_soplo  ,sizeEyectivoB_soplo  ,m_EyectivoB_soplo ],
                  "equation_Protomesosistolico":[MaxValueProtomesositolico_soplo  ,sizeProtomesositolico_soplo ,m_Protomesositolico_soplo ],
                  "equation_Telesistolico":[MaxValueTelesistolico_soplo  ,sizeTelesistolico_soplo  ,m_Telesistolico_soplo ],
                  "equation_Protodiastolico":[MaxValueProtodiastolico_soplo  ,sizeProtodiastolico_soplo  ,m_Protodiastolico_soplo ],
                  "equation_MesodiastolicoA":[MaxValueMesodiastolicoA_soplo  ,sizeMesodiastolicoA_soplo  ,m_MesodiastolicoA_soplo ],
                  "equation_MesodiastolicoB":[MaxValueMesodiastolicoB_soplo  ,sizeMesodiastolicoB_soplo  ,m_MesodiastolicoB_soplo ],
                  "equation_Telediastolico":[MaxValueContinuo_soploA  ,sizePamsistolico_soplo ,m_Telediastolico_soplo ],
                  "equation_ContinuoA":[MaxValuePamsistolico_soplo ,sizeContinuo_soploA  ,m_Continuo_soploA ],
                  "equation_ContinuoB":[MaxValueContinuo_soploB  ,sizeContinuo_soploB  ,m_Continuo_soploB ],
                  }
                  
def CalculateSlope(adcY_offset,y0,x0,size,orientation):
    slope = 0
    slope = (y0 - adcY_offset)/(size)
    if orientation == -1:
        slope = -1*(slope)
    return slope

def CreateSignal(aSequences):
    signal =[0]*N
    step = 0
    sequence_size = len(aSequences)
    size_index = 1
    sequence_i = 0
    x0 =0
    
    #start state machine commands
    command = aSequences[step]
    #Parameters  key:[MaxValue,size,slope orientation]
    parameters  = ParametersDict[command]
    MapPoint_function = functionsDict[command]
    slope = CalculateSlope(adcY_offset,parameters[0],x0,parameters[1],parameters[2])

    for i in xrange(N):
        #control the state machine, the state change occurs when the size of the signal has been reached 
        if(sequence_i >= parameters[size_index]): # move to the next step 
            step+=1
            sequence_i = 0
            x0 = i
            if step >= sequence_size :#if the signal reach the last step , start again 
                step = 0 
                
            command = aSequences[step]
            #Parameters  key:[MaxValue,size,slope orientation]
            parameters  = ParametersDict[command]
            MapPoint_function = functionsDict[command]
            slope = CalculateSlope(adcY_offset,parameters[0],x0,parameters[1],parameters[2])
                               
        sequence_i+=1
        #Parameters  key:[offset_adc_Y/slope,MaxValue,x0]
        #MapPoint_function(point_x,offset_adcY,MaxValue,x0,slope)
        signal[i] = MapPoint_function(i,adcY_offset,parameters[0],x0,slope)   
    
    plt.plot(signal)
    plt.show()

    #print signal
    return signal
    
#take care that the sequence can be fit in the signal 
test_sequence_random =["random_R4","random_R2R1","random_R1","random_R1R2","random_R2","random_SmallSpace"]

test_sequence_equation = ["equation_R2R1","equation_R1","equation_R1R2","equation_R2"]

test_sequence_pamsistolico = ["random_R1","random_Pamsistolico","random_R2","random_R2R1"]

test_sequence_eyectivo = ["random_R1","random_SmallSpace","equation_EyectivoA","equation_EyectivoB",
                            "random_R2","random_R2R1"]
                            
test_sequence_protomesosistolico = ["random_R1","equation_Protomesosistolico","random_BigSpace",
                            "random_R2","random_R2R1"]    
                            
test_sequence_telesistolico = ["random_R1","random_BigSpace","equation_Telesistolico",
                            "random_R2","random_R2R1"]  

test_sequence_protodiastolico = ["random_R1","random_R1R2",
                            "random_R2","equation_Protodiastolico","random_BigSpace"]   

test_sequence_Mesodiastolico = ["random_R1","random_R1R2",
                            "random_R2","random_SmallSpace","equation_MesodiastolicoA","equation_MesodiastolicoB","random_SmallSpace"]     

test_sequence_Telediastolico = ["random_R1","random_R1R2",
                            "random_R2","random_BigSpace","equation_Telediastolico"]                            

test_sequence_Continuo = ["random_R1","equation_ContinuoA" ,"random_R2","equation_ContinuoB","random_R2R1"]

adcCardiophonogram = CreateSignal(test_sequence_Mesodiastolico)
movinAverage_filter = FilterMovingAverage(5)
filtered_signal = movinAverage_filter.dspfilter(adcCardiophonogram)
plt.plot(filtered_signal)
plt.show()
'''
S=1000
N=385 #multiplo de 2 - kernel +1
M=128
#sig=[4]*N
sig=[]
x=[];
buff=[]
x1=[];

#signal 10 hz muestreada a 1 khz
for i in xrange(0,S,1):
   sig.append(5*(cos(2*pi*i*400/S)))
   x.append(1.0*i/S)


plt.axis( [0, 1, -10, 10])
plt.plot(x1,output)
plt.ylabel('filter')
plt.show()'''


