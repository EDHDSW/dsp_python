from DSPs import*
import matplotlib.pyplot as plt
'''
Create a window sinc filter
'''
S=1000
N=385 #multiplo de 2 - kernel +1
M=128
#sig=[4]*N
sig=[]
x=[];
buff=[]
x1=[];

#signal 10 hz muestreada a 1 khz
for i in xrange(0,S,1):
   sig.append(5*(cos(2*pi*i*400/S)))
   x.append(1.0*i/S)
   
#buffer
for i in xrange(0,N,1):
    buff.append(sig[i])
    x1.append(x[i]) 

windowsinc=FilterWindowSinc()

#windowsinc.setlowpass(.05,M)

windowsinc.sethighpass(.4,M)

#windowsinc.setbandpass(.0,.5,M)

#windowsinc.setbandreject(.,.1,M)

#output= windowsinc.dspfilter(buff)

output = windowsinc.FFTFilter(buff)

plt.axis( [0, 1, -10, 10])
plt.plot(x,sig)
plt.ylabel('signal')
plt.show()


plt.axis( [0, 1, -10, 10])
plt.plot(x1,output)
plt.ylabel('filter')
plt.show()

