# -*- coding: utf-8 -*-
"""
Created on Sun Jul 07 20:16:52 2013

@author: EDHDSW
window sync EMG sampling at 1000 hz
creating two sinuoids of 17  with noise of 60hz caused by the electrical suply

"""
import DSPs
import math
import matplotlib.pyplot as plt

S=1000 #sampling rate
NN= 5000 #numero de puntos
M= 256 # filter size BW=BW=4/M

sig17=[]
sig60=[]
EMG=[]
x=[]
buff=[]
x1=[]
data=[]

#create the signals
#https://math.stackexchange.com/questions/1501602/sine-wave-formula
#signal 17 and 60 hz muestreada a 1000 hz
for i in xrange(0,S,1):
    # 3 = Amplitute ,17 = Frequency , s =sampling rate 
    sig17.append(3*(math.cos(2*math.pi*i*17/S))) 
    sig60.append(10*(math.sin(2*math.pi*i*60/S)))
    EMG.append( sig17[i] + sig60[i])
    x.append(1.0*i)
    
#fill the buffer with the signal created
for i in xrange(0,NN/S,1): 
    data=data+EMG
 
windowsinc=DSPs.FilterWindowSinc()
 
windowsinc.setlowpass(0.040,M) # frecuency cut its the frecuency cut plus the roll BW = 0.02

#filtramos este numero de muestras para poder utilizar FFT
output = windowsinc.FFTFilter(data[:3841])

#SPECTRUMS
#calc x1
N=1024
spec1=[]
spec=[]
for i in xrange(0,N/2,1):
    x1.append(1.0*i/N)
#spectrum filter
spec = DSPs.Spectrum(output[:N],output[:N],N,NN)

#spectrum original 
spec1 = DSPs.Spectrum(data[:N],data[:N],N,NN)

f,plots=plt.subplots(6,1)

plots[0].plot(x,sig17)
plots[0].set_title('Sensor signal 17 hz')

plots[1].plot(x,sig60)
plots[1].set_title('Sensor signal 60 hz')

plots[2].plot(x,data[:S])
plots[2].set_title('Sensor signal')

plots[3].plot(x,output[:S])
plots[3].set_title('Filter Signal')

plots[4].plot(x1,spec1)
plots[4].set_title('Spectrum Sensor Signal')

plots[5].plot(x1,spec)
plots[5].set_title('Spectrum Filter Signal')
plt.show()

#eexpand the plot size in ipython pylab.rcParams['figure.figsize'] = (10.0, 8.0)



