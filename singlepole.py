# -*- coding: utf-8 -*-
"""
Created on Sun Jul 07 21:58:59 2013

@author: EDHDSW
"""

import DSPs
import math
import matplotlib.pyplot as plt

#x =exp(-2*pi*fc
N= 256
S= 1000
#sig=[4]*N
sig=[]
#https://math.stackexchange.com/questions/1501602/sine-wave-formula
for i in xrange(0,S,1):
   sig.append(5*(math.cos(2*pi*i*40/S)))
x= math.exp(-2.0*math.pi*.1)
filter1=DSPs.FilterSinglePole()
#filter1.setlowpass(x)
#filter1.sethighpass(x)
filter1.setlowpass4stage(x)
#output=filter1.dspfilter(sig)
output =filter1.dspfilterbidirectional(sig)

plt.plot(sig)
plt.plot(output)
plt.ylabel('')
plt.show()
