﻿import  DSPs 
import math
import numpy as np
import matplotlib.pyplot as plt
N=1024
S=16000

#real=[4]*N + [0]* 56
#img=[0]*N+ [0]* 56
real=[]
img=[]
spec=[]
x=[]
for i in xrange(0,N,1):
   real.append(5.0*math.cos( 2*math.pi*i*6400/S))
   real[i]= real[i]+(5.0*math.cos( 2*math.pi*i*1600/S))
   real[i]= real[i]+(5.0*math.cos( 2*math.pi*i*600/S))
   img.append(5.0*math.cos( 2*math.pi*i*6400/S))
   img[i]= img[i]+(5.0*math.cos( 2*math.pi*i*1600/S))
   img[i]= img[i]+(5.0*math.cos( 2*math.pi*i*600/S))
'''
plt.plot(real)
plt.ylabel('real')
plt.show()
'''

DSPs.FFT(real,img,-1)

#spectrum
for i in xrange(0,N/2,1):
    #IN(ω) =1/N*|Y (ω)|**2
    spec.append(((real[i]**2)+(img[i]**2)) )
    #ormalization
    spec[i]=spec[i]/(N*2)#sampling rate *number of spectrums buffers added
    x.append(1.0*i/N)

plt.plot(x,spec)
plt.ylabel('spectrum')
plt.show()

